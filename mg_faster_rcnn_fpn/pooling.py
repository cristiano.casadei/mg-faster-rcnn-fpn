from typing import List

import torch
from torch import Tensor
from torch.nn.functional import grid_sample


@torch.jit.script
def linspace(start: Tensor, stop: Tensor, num: int):
    """
    Creates a tensor of shape [num, *start.shape] whose values are evenly spaced from start to end, inclusive.
    Replicates the multi-dimensional bahaviour of numpy.linspace in PyTorch.
    """
    # create a tensor of 'num' steps from 0 to 1
    steps = torch.arange(num, dtype=torch.float32, device=start.device) / (num - 1)

    # reshape the 'steps' tensor to [-1, *([1]*start.ndim)] to allow for broadcastings
    # - using 'steps.reshape([-1, *([1]*start.ndim)])' would be nice here but torchscript
    #   "cannot statically infer the expected size of a list in this contex", hence the code below
    for i in range(start.ndim):
        steps = steps.unsqueeze(-1)

    # the output starts at 'start' and increments until 'stop' in each dimension
    return start[None] + steps * (stop - start)[None]


@torch.jit.script
def roi_grid(rois: Tensor, size: int):
    """
    Using rois of shape [N, 4, 2 (x, y)] as input, returns a
    sampled grid of [size (y), size (x)] points within each roi.
    Output has shape [N, size (y), size (x), 2 (x, y)].
    """
    # interpolate two opposite edges of each roi
    idx_edge_1 = linspace(start=rois[:, 1], stop=rois[:, 2], num=size)
    idx_edge_2 = linspace(start=rois[:, 0], stop=rois[:, 3], num=size)

    # interpolate between the edges of each roi
    # reshape the output from [size_x, size_y, N, 2] to [N, size_y, size_x, 2]
    return linspace(start=idx_edge_1, stop=idx_edge_2, num=size).permute([2, 1, 0, 3])


@torch.jit.script
def roi_pool(tensor: Tensor, rois: Tensor, size: int):
    """
    Pool a square arond each ROI from 'tensor'.
    For an ROI of size [w, h], a square of size
    [s, s] is pooled, where s = max(w, h).
    'rois' must be in range [0, 1], have shape [N, 4, 2 (x, y)] and
    be ordered along the second axis (clockwise or anticlockwise).
    """
    # compute coordinates of the squares
    w = torch.amax(rois[:, :, 0], 1) - torch.amin(rois[:, :, 0], 1)
    h = torch.amax(rois[:, :, 1], 1) - torch.amin(rois[:, :, 1], 1)
    hw = torch.stack((torch.stack((w / 2, h / 2)),
                      torch.stack((-w / 2, h / 2)),
                      torch.stack((-w / 2, -h / 2)),
                      torch.stack((w / 2, -h / 2)))).permute((2, 0, 1))
    c = torch.mean(rois, 1, keepdim=True).repeat(1, 4, 1) + hw

    # compute a grid of indices for each roi
    # translate the grid from [0, 1] to [-1, 1] coordinates
    rois_interpolated = ((roi_grid(c, size) * 2) - 1).to(tensor.dtype)

    # gather the values indexed by the ROI grids
    return torch.stack([grid_sample(tensor[None], r[None], align_corners=True)[0] for r in rois_interpolated])


@torch.jit.script
def get_level_idx(features: List[Tensor], rois: Tensor,
                  scale_factor: int = 4,
                  k_min: int = 2,
                  k_max: int = 5,
                  lvl_0: int = 4):
    """
    The original pooling heuristic proposed in "Feature Pyramid Networks for Object Detection". 
    """
    # get the approximate res of input image
    image_h = scale_factor * features[0].shape[2]
    image_w = scale_factor * features[0].shape[3]

    # get the res of each roi in relative units
    w = rois[:, :, 0].amax(1) - rois[:, :, 0].amin(1)
    h = rois[:, :, 1].amax(1) - rois[:, :, 1].amin(1)

    # get the res of each roi in terms of input image
    w = w * image_w
    h = h * image_h

    # calculate the target level idx for each roi using the heuristic proposed in the fpn paper
    k = lvl_0 + torch.log2(torch.sqrt(w * h) / 224.)
    # return the target layer as indexed in a list
    return k.floor().int().clamp(k_min, k_max) - k_min


@torch.jit.script
def pool_fpn_features(features: List[Tensor], rois: Tensor, size: int):
    """
    Pool quadrilateral ROIs from a feature pyramid.
    'rois' must be in range [0, 1], have shape [N, 4, 2 (x, y)] and
    be ordered along the second axis (clockwise or anticlockwise).
    """
    # calculate the target level idx for each roi
    roi_pooling_level = get_level_idx(features, rois)

    # pool the selected levels
    c = features[0].shape[1]
    pooled_rois = torch.zeros((rois.shape[0], c, size, size), device=features[0].device, dtype=features[0].dtype)
    for level, level_features in enumerate(features):
        # select those rois with a scale corresponding to current level
        idx_in_level = torch.nonzero(level == roi_pooling_level).squeeze(1)
        if idx_in_level.shape[0] > 0:
            rois_per_level = rois[idx_in_level]
            pooled_rois[idx_in_level] = roi_pool(level_features[0], rois_per_level, size)

    return pooled_rois
