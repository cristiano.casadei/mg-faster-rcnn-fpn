import torch

from mg_faster_rcnn_fpn.faster_rcnn_fpn import FasterRCNN_FPN

import argparse

p = argparse.ArgumentParser()
p.add_argument("--weights-in", type=str, help="Input weights (state dict)", required=True)
p.add_argument("--weights-out", type=str, help="Output weights (inference weights)", required=True)
args = p.parse_args()

print("Create model")
model = FasterRCNN_FPN()
print("Load state dict weights")
model.load_state_dict(torch.load(args.weights_in, map_location='cpu'))
print("Prepare for evaluation")
model.eval()
print("Save inference weights")
torch.save(model, args.weights_out)
print("Done")
