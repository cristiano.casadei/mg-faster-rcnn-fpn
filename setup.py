from setuptools import find_packages
from distutils.core import setup

# leggo i requirements centralizzati
with open("./requirements.txt", "r") as f:
    requirements = str.strip(str(f.read())).split("\n")

setup(
    name="mg-faster-rcnn-fpn",
    version="1.0.0",
    packages=find_packages(),
    install_requires=requirements
)
